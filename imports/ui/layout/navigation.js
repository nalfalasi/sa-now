import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
// import routes from "../../startup/client/route"

const routes = [
  {
    path: "/",
    exact: true,
    main: () => <h2>Home</h2>
  },
  {
    path: "/DashBoard",
    main: () => <h2>DashBoard</h2>
  },
  {
    path: "/Profile",
    main: () => <h2>Profile</h2>
  }
];
const Navi = () => (

<Router>
    <div style={{ display: "flex" }}>
      <Card
        style={{
          padding: "10px",
        width: "200px",
         height: "1000px",

        }}
      >
        <ul style={{ listStyleType: "none", padding: 0 }}>
          <li>
          <Button variant="outlined" size="medium" color="primary" >
          <Link to="/">Home</Link>
        </Button>
          
          </li>
          <li>
          <Button variant="outlined" size="medium" color="primary" >
          <Link to="/DashBoard">DashBoard</Link>
        </Button>
          </li>
          <li>
         <Button variant="outlined" size="medium" color="primary" >
                 <Link to="/Profile">Profile</Link>
        </Button>
        
          </li>
        </ul>

        {routes.map((route, index) => (
          // You can render a <Route> in as many places
          // as you want in your app. It will render along
          // with any other <Route>s that also match the URL.
          // So, a sidebar or breadcrumbs or anything else
          // that requires you to render multiple things
          // in multiple places at the same URL is nothing
          // more than multiple <Route>s.
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.sidebar}
          />
        ))}
      </Card>

      <div style={{ flex: 1, padding: "10px" }}>
        {routes.map((route, index) => (
          // Render more <Route>s with the same paths as
          // above, but different components this time.
          <Route
            key={index}
            path={route.path}
            exact={route.exact}
            component={route.main}
          />
        ))}
      </div>
    </div>
  </Router>
  
  );
  
  export default Navi;
  
