import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Grid from '@material-ui/core/Grid';


export default class TopBar extends Component {

    

  state = {
    icons: 0,
  }

  
  render() {
    return (  
<AppBar position="static" >
  <Toolbar>
    <Grid
      justify="space-between" // Add it here :)
      container 
      spacing={24}
    >
      <Grid item>
      <IconButton color="inherit">
      <MenuIcon></MenuIcon>
      </IconButton>
  

      </Grid>
      <Grid item>
        <Typography type="title" color="inherit">
        <h1>
        San Francisco Now
        </h1>
         
        </Typography>
      </Grid>

      <Grid item>
        <div>
   
          <Button raised color="inherit">
            LogOut
          </Button>
        </div>
      </Grid>
    </Grid>
  </Toolbar>
</AppBar>
      
    );
  }
}
