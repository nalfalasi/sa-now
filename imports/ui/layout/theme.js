import { createMuiTheme } from 'material-ui/styles';
// import indigo from 'material-ui/colors/indigo';
// import blue from 'material-ui/colors/blue';
// import red from 'material-ui/colors/red';
import { grey100, blue100 } from 'material-ui/styles/colors';

const theme =  createMuiTheme({
  palette: {
    primary: blue100,
    secondary: grey100 // Indigo is probably a good match with pink
  }
});

export default theme